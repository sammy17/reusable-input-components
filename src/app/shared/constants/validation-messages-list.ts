export const REQUIRED_FIELD_MESSAGE = 'This field is required';

/** Patterns messages: */
export const EMAIL_PATTERN_MESSAGE = 'Incorrect Email';
export const CYRILLIC_PATTERN_MESSAGE = 'Only cyrillic';
export const ALPHA_NUMERIC_MESSAGE = 'Only AlphaNumeric';

