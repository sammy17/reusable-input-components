import { Injectable }                            from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {
  /**
   * Method 'markAllFormFieldsAsTouched' validates if all control of the input form group are valid.
   * If not, it marks them and the entire form touched to trigger required validation procedures.
   */
  markAllFormFieldsAsTouched(form: AbstractControl): void {
    form.markAsTouched({onlySelf: true});
    if (form instanceof FormArray || form instanceof FormGroup) {
      Object.values(form.controls).forEach(this.markAllFormFieldsAsTouched);
    }
  }
  forbiddenTextPattern(control: FormControl): {[s: string]: boolean}{
    if(control.value !== null  ||  control.value!== undefined || control.value!==''){
      // console.log(typeof(control.value),control.value)
        let oldValue:string=JSON.stringify(control.value)
        let newValue=oldValue;
        oldValue=oldValue.replace(/(<([^>]+)>)/gi,"")
        if(! (oldValue === newValue))
          return {'forbiddenTextPattern': true};
        return null;
    }
    return null;
         
  }
  
}
