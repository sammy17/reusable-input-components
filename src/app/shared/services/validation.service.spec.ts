import { TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ValidationService } from './validation.service';


fdescribe('Validation Service', () => {
    let service: ValidationService;
    let testForm:FormGroup;
  beforeEach(() => {
    service = TestBed.inject(ValidationService);
  });

  it('[Validation-check] - should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('[Validation-check]',()=>{
    let formControl;
    beforeAll(()=> {
      
      testForm = new FormGroup({
        'userEmail': new FormControl(
          null,
         [Validators.required,]
      )});
    });

    beforeEach(()=>{
      formControl = testForm.controls['userEmail'];
    });

    it('[Validation-check] - check function markAllFormFieldsAsTouched',()=> {
        service.markAllFormFieldsAsTouched(testForm);
        expect(testForm.touched).toBeTruthy();
    });

    it('[Validation-check] - check function markAllFormFieldsAsTouched for required value error',()=> {
      service.markAllFormFieldsAsTouched(testForm);
      formControl.setValue("");
      expect(formControl.errors['required']).toBeTruthy()
    });

    it('[Validation-check] - check function forbiddenTextPattern with invalid value',()=> {
        formControl.setValue("<script>")
        let error = service.forbiddenTextPattern(formControl);
        expect(error).toEqual({'forbiddenTextPattern': true});
    });

    it('[Validation-check] - check function forbiddenTextPattern with valid value',()=> {
        formControl.setValue("test")
        let error = service.forbiddenTextPattern(formControl);
        expect(error).toEqual(null);
    });
  });
});