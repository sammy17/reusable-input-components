import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-input-date-time-picker',
  templateUrl: './input-date-time-picker.component.html',
  styleUrls: ['./input-date-time-picker.component.scss']
})
export class InputDateTimePickerComponent implements OnInit {
  @Input() fieldId:string;
  @Input() label:string;
  @Input() control:AbstractControl;
  @Input() type:'datetime-local'
  @Input() defaultDateTime:string
  constructor() { }

  ngOnInit(): void {
  }

}
