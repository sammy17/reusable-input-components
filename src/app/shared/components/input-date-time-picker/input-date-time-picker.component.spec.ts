import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveValidationComponent } from '../reactive-validation/reactive-validation.component';
import { InputDateTimePickerComponent } from './input-date-time-picker.component';

describe('InputDateTimePickerComponent', () => {
  let component: InputDateTimePickerComponent;
  let fixture: ComponentFixture<InputDateTimePickerComponent>;
  let testForm;
  let htmlCompiled;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ RouterTestingModule, ReactiveFormsModule ],
      declarations: [ InputDateTimePickerComponent,ReactiveValidationComponent ]
    })
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputDateTimePickerComponent);
    component = fixture.debugElement.componentInstance;
    htmlCompiled = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  describe('DateTime input field',()=>{
    let dateTimeControl;
    let dateTimeInput;

    beforeAll(()=> {
      testForm = new FormGroup({
        'dateTime': new FormControl(
          null,
          [ Validators.required ]
      )});
    });

    beforeEach(()=>{
      component.label = 'dateTime';
      component.control = testForm.controls['dateTime'];
      component.defaultDateTime= '2018-06-12T19:30'
      component.type = 'datetime-local';
      component.fieldId = 'dateTime';
      dateTimeControl = component.control;
      component.ngOnInit();
      fixture.detectChanges();
      dateTimeInput = htmlCompiled.querySelector('input');
    });
    
    it('[DateTime-check] - check HTML values',()=> {
      expect(dateTimeInput).toBeTruthy();
      let label = htmlCompiled.querySelector('label').textContent.trim();
      expect(label).toEqual(component.label);
      expect(dateTimeInput.id).toEqual(component.fieldId);
      expect(dateTimeInput.type).toEqual(component.type);
      dateTimeControl.setValue('2018-06-12T19:30');
      expect(dateTimeInput.value).toEqual(dateTimeControl.value);
      expect(dateTimeInput.value).toEqual(component.defaultDateTime)
    });

    it('[Date-check] - invalid blank value',()=>{
      dateTimeControl.setValue('');
      expect(dateTimeControl.errors['required']).toBeTruthy();
      expect(dateTimeControl.invalid).toBeTruthy();
    });
 
    it('[Date-check] - invalid value',()=>{
      component.defaultDateTime = "123:123"
      fixture.detectChanges()
      expect(component.defaultDateTime).not.toEqual(dateTimeInput.value);
      expect(dateTimeInput.value).toEqual('')
    });

    //valid testcase
    it('[Date-check] - valid value',()=>{
      component.defaultDateTime = "2018-06-12T19:30"
      fixture.detectChanges()
      expect(component.defaultDateTime).toEqual(dateTimeInput.value);
      expect(dateTimeInput.value).toEqual('2018-06-12T19:30')
    });
  });
});




