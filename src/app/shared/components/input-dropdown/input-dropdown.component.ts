import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-input-dropdown',
  templateUrl: './input-dropdown.component.html',
  styleUrls: ['./input-dropdown.component.scss']
})
export class InputDropdownComponent implements OnInit {
@Input() values:string[];
@Input() control:AbstractControl;
@Input() fieldId:string;
@Input() Selected:string;
@Input() label:string;
  constructor() { }

  ngOnInit(): void {
  }

}
