import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveValidationComponent } from '../reactive-validation/reactive-validation.component';

import { InputDropdownComponent } from './input-dropdown.component';

describe('InputDropdownComponent', () => {
  let component: InputDropdownComponent;
  let fixture: ComponentFixture<InputDropdownComponent>;
  let testForm;
  let htmlCompiled;
  beforeEach(async () => {
     TestBed.configureTestingModule({
      imports: [ RouterTestingModule, ReactiveFormsModule ],
      declarations: [ InputDropdownComponent, ReactiveValidationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputDropdownComponent);
    component = fixture.debugElement.componentInstance;
    htmlCompiled = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
 
  describe('dropdown input field',()=>{
    let dropdownControl;
    let dropdownInput;

    beforeAll(()=> {
      testForm = new FormGroup({
        'dropdown': new FormControl(
          null,
          [ Validators.required ]
      )});
    });

    beforeEach(()=>{
      component.label = 'Dropdown';
      component.control = testForm.controls['dropdown'];
      component.fieldId = 'dropdown';
      component.values=["Save Data","Fetch Data","Delete Data"]
      dropdownControl = component.control;
      component.ngOnInit();
      fixture.detectChanges();
      dropdownInput = htmlCompiled.querySelector('select');

    });

    it('[dropdown-check] - check HTML values',()=> {
      expect(dropdownInput).toBeTruthy();
      let label = htmlCompiled.querySelector('label').textContent.trim();
      expect(label).toEqual(component.label);
      expect(dropdownInput.id).toEqual(component.fieldId);
    });

    it('[dropdown-check] - invalid blank value',()=>{
      dropdownControl.setValue('');
      expect(dropdownControl.errors['required']).toBeTruthy();
      expect(dropdownControl.invalid).toBeTruthy();
    });

    it('[dropdown-check] - valid values in dropdown',()=>{
      dropdownControl.setValue(component.values);
      let selectedIndex = 0;
      expect(dropdownInput.options[selectedIndex].value).toEqual(dropdownControl.value[0]);
      selectedIndex = 1;
      expect(dropdownInput.options[selectedIndex].value).toEqual(dropdownControl.value[1]);
      selectedIndex = 2;
      expect(dropdownInput.options[selectedIndex].value).toEqual(dropdownControl.value[2]);
    });

    it('[dropdown-check] - verify label value',()=>{
      component.label = "DropDown"
      fixture.detectChanges();
      expect(htmlCompiled.querySelector('label').textContent).toEqual("DropDown");
    });

  });

});
