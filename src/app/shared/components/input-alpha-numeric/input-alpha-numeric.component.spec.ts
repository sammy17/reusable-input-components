import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveValidationComponent } from '../reactive-validation/reactive-validation.component';
import { ALPHA_NUMERIC_PATTERN } from 'src/app/shared/constants/validation-patterns-list';
import { InputAlphaNumericComponent } from './input-alpha-numeric.component';
import { ValidationService } from '../../services/validation.service';

describe('Component: Reactive- AlphaNumeric Input', () => {
    let component: InputAlphaNumericComponent;
    let fixture: ComponentFixture<InputAlphaNumericComponent>;
    let testForm;
    let htmlCompiled;

    beforeEach( async() => {
      TestBed.configureTestingModule({
        imports: [ RouterTestingModule, ReactiveFormsModule ],
        declarations: [ InputAlphaNumericComponent, ReactiveValidationComponent ],
      })
      
    });

    beforeEach(() => {
      fixture = TestBed.createComponent(InputAlphaNumericComponent);
      component = fixture.debugElement.componentInstance;
      htmlCompiled = fixture.debugElement.nativeElement;
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    describe('AlphaNumeric input field',()=>{
      let alphaNumericControl;
      let alphaNumericInput;

      beforeAll(()=> {
        testForm = new FormGroup({
          'alphaNumeric': new FormControl(
            null,
            [ Validators.required, Validators.pattern(ALPHA_NUMERIC_PATTERN) ]
        )});
      });

      beforeEach(()=>{
        component.label = 'AlphaNumeric';
        component.control = testForm.controls['alphaNumeric'];
        console.log(component.control)
        component.type = 'text';
        component.fieldId = 'alphaNumeric';
        alphaNumericControl = component.control;
        component.ngOnInit();
        fixture.detectChanges();
        alphaNumericInput = htmlCompiled.querySelector('input');

      });
      it('[AlphaNumeric-check] - check HTML values',()=> {
        expect(alphaNumericInput).toBeTruthy();
        let label = htmlCompiled.querySelector('label').textContent.trim();
        expect(label).toEqual(component.label);
        expect(alphaNumericInput.id).toEqual(component.fieldId);
        expect(alphaNumericInput.type).toEqual(component.type);
        alphaNumericControl.setValue('testAlphaNumeric');
        expect(alphaNumericInput.value).toEqual(alphaNumericControl.value);
      });

      it('[AlphaNumeric-check] - invalid blank value',()=>{
        alphaNumericControl.setValue('');
        expect(alphaNumericControl.errors['required']).toBeTruthy();
        expect(alphaNumericControl.invalid).toBeTruthy();
      });


      it('[AlphaNumeric-check] - invalid textPattern due to special character',()=>{
        alphaNumericControl.setValue('testAlphaNumeric@');
        expect(alphaNumericControl.errors['pattern']).toBeTruthy();
        expect(alphaNumericControl.invalid).toBeTruthy();
      });

      it('[AlphaNumeric-check] - valid textPattern ',()=>{
        alphaNumericControl.setValue('testcom1234');
        expect(alphaNumericControl.valid).toBeTruthy();
        expect(alphaNumericControl.errors).toBeNull();
      });

      it('[AlphaNumeric-check] - valid textPattern ',()=>{
        alphaNumericControl.setValue('testcom1234');
        expect(alphaNumericControl.valid).toBeTruthy();
        expect(alphaNumericControl.errors).toBeNull();
      });
      //script error testcase

      it('[Simple input-check] - invalid script tags',()=>{
        alphaNumericControl.setValue('<script>');
        expect(alphaNumericControl.invalid).toBeTruthy();
        expect(alphaNumericControl.errors['pattern']).toBeTruthy();
      });
    });


});

