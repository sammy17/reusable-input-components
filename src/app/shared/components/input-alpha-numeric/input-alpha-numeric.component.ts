import { Component, OnInit,Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-input-alpha-numeric',
  templateUrl: './input-alpha-numeric.component.html',
  styleUrls: ['./input-alpha-numeric.component.scss']
})
export class InputAlphaNumericComponent implements OnInit {

  @Input() label: string | null = null;
  @Input() fieldId: string | null = null;
  @Input() type: 'text' | 'email' | 'password' = 'text';
  @Input() control: AbstractControl | null = null;

  validationErrors: object = null;

  ngOnInit() {}

  ngDoCheck() {
    
    this.validationErrors = this.control.touched && this.control.invalid ? this.control['errors'] : null;
    console.log(this.validationErrors)
  }
}
