import { ChangeDetectionStrategy, Component, Input, OnChanges } from '@angular/core';
import { REQUIRED_FIELD_MESSAGE }                               from '../../constants/validation-messages-list';
import { PATTERNS_LIST }                                        from '../../constants/validation-patterns-list';

@Component({
  selector: 'app-reactive-validation',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './reactive-validation.component.html',
  styleUrls: ['./reactive-validation.component.scss']
})
export class ReactiveValidationComponent implements OnChanges {

  /**
   * 'validationErrors' is reactive form errors.
   * It's nonnull in case if the control is touched and invalid,
   * which is defined on the reactive base component side.
   */
  @Input() validationErrors: object | null = null;

  errorMessage: string | null = null;

  ngOnChanges(): void {
    this.errorMessage = this.getErrorMessage();
  }

  getErrorMessage(): string | null {
    const errors = this.validationErrors;
    console.log(this.validationErrors)
    if (errors) {
      return errors['required'] ? REQUIRED_FIELD_MESSAGE                            /** <----------- Data should be filled     */
        : errors['pattern'] ? this.getPatternMessage(errors['pattern']['requiredPattern'])  /** <----------- Data should match pattern */
        : errors['forbiddenTextPattern']? 'Do not use tags'                                  /** <------------Data should not be js element*/ 
        : errors['email']? 'Email is not correct'
        : null;                                                                           /** <----------- Data is filled correctly  */
    }
    return null;
  }

  /**
   * Method 'getPatternMessage' finds proper pattern message form patterns list
   * and returns the message.
   */
  getPatternMessage(requiredPattern: string): string {
    return PATTERNS_LIST.filter(x => x['PATTERN'] === requiredPattern)[0]['MESSAGE'];
  }
}
//rate calculator to input text and write test cases
//label component gitlab account