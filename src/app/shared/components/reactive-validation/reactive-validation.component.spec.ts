import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveValidationComponent } from '../reactive-validation/reactive-validation.component'
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ValidationService } from '../../services/validation.service'
import { REQUIRED_FIELD_MESSAGE }                               from '../../constants/validation-messages-list';
import { By } from '@angular/platform-browser';


describe('Component: Reactive-Validation',()=>{
    let fixture: ComponentFixture<ReactiveValidationComponent>;
    let component: ReactiveValidationComponent;
    let spanText;
    let htmlCompiled;

    beforeEach(async()=>{
      TestBed.configureTestingModule({
        imports: [ RouterTestingModule, ReactiveFormsModule ],
        declarations: [ ReactiveValidationComponent ],
        providers: [ ValidationService ]
      })
    })

    beforeEach(()=>{
        fixture =  TestBed.createComponent(ReactiveValidationComponent);
        component = fixture.debugElement.componentInstance;
        htmlCompiled = fixture.debugElement.nativeElement;
        spanText = fixture.debugElement.query(By.css('span'));
      });

    it('should create reactive validation',()=> {
       expect(component).toBeTruthy();
    });

    it('[Validation-Check] - span tag not present due to null value of error ',()=> {
        component.validationErrors = null;
        fixture.detectChanges();
        expect(spanText).toEqual(null);
    });

     it('[Validation-Check] -  error value to be "required" ',()=> {
      component.validationErrors = {'required':true};
      component.ngOnChanges();
      fixture.detectChanges();
      spanText = fixture.debugElement.query(By.css('span')).nativeElement;
      console.log(spanText)
      expect(spanText.textContent.trim()).toEqual(REQUIRED_FIELD_MESSAGE);
    });

     it('[Validation-Check] - error value to be "email"',()=> {
      component.validationErrors = {'email':true};
      component.ngOnChanges();
      fixture.detectChanges();
      spanText = fixture.debugElement.query(By.css('span')).nativeElement;
      console.log(spanText)
      expect(spanText.textContent.trim()).toEqual('Email is not correct');
    });

    it('[Validation-Check] - error value to be "email"',()=> {
      component.validationErrors = {'forbiddenTextPattern':true};
      component.ngOnChanges();
      fixture.detectChanges();
      spanText = fixture.debugElement.query(By.css('span')).nativeElement;
      expect(spanText.textContent.trim()).toEqual('Do not use tags');
    });

    it('[Validation-Check] - error value to be alphaNumeric pattern',()=> {
      component.validationErrors = {'pattern':{'requiredPattern':"^[a-zA-Z0-9 '-]+$"}};
      component.ngOnChanges();
      fixture.detectChanges();
      spanText = fixture.debugElement.query(By.css('span')).nativeElement;
      expect(spanText.textContent.trim()).toEqual('Only AlphaNumeric');
    });

    it('[Validation-Check] - error value to be forbiddenText pattern',()=> {
      component.validationErrors = {'forbiddenTextPattern':true};
      component.ngOnChanges();
      fixture.detectChanges();
      spanText = fixture.debugElement.query(By.css('span')).nativeElement;
      expect(spanText.textContent.trim()).toEqual('Do not use tags');
    });
});

