import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RateCalculatorContainerComponent } from 'src/app/modules/rate-calculator/rate-calculator-container/rate-calculator-container.component';

import { ButtonComponent } from './button.component';

describe('ButtonComponent', () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;
  let buttonHTML;
  let rateComponent : RateCalculatorContainerComponent
  beforeEach(async () => {
      TestBed.configureTestingModule({
        imports: [
          ReactiveFormsModule
        ],
      declarations: [ ButtonComponent,RateCalculatorContainerComponent  ],
      providers:[]
    })
  
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.debugElement.componentInstance;
    buttonHTML = fixture.debugElement.nativeElement.querySelector('button');
    rateComponent = TestBed.createComponent(RateCalculatorContainerComponent).debugElement.componentInstance

  });

  it('should create component Button', () => {
    expect(component).toBeTruthy();
  });

  it('[ Button-Click ] - function should be called', (() => {
    spyOn(component,"onClickButton")
    buttonHTML.click();
    expect(component.onClickButton).toHaveBeenCalled();
  }));

  it('[ Button-Click ] - event should be emitted on Submit', (() => {
   spyOn(component.onClick, 'emit');
   component.onClickButton()
   fixture.detectChanges()
   expect(component.onClick.emit).toHaveBeenCalled();
  }));
 
  it('[ Button-Click ] - event should be emitted on ButtonClick', (() => {
    spyOn(component.onClick, 'emit');
    buttonHTML.click();
    fixture.detectChanges()
    expect(component.onClick.emit).toHaveBeenCalled();
  }));

  it('[ Button-Text ] - text should be there', (() => {
    component.label = "Submit"
    fixture.detectChanges()
    console.log(buttonHTML)
    expect(buttonHTML.textContent).toEqual('Submit');
  }));

  it('[ Button-Type ] - type should be button', (() => {
    component.type='button';
    component.label = 'Submit'
    fixture.detectChanges()
    expect(buttonHTML.type).toEqual('button');
  }));

  it('[ Button-Click ] - disabled button "true" ', (() => {
    component.disabledCondition = true;
    fixture.detectChanges()
    expect(buttonHTML.disabled).toBeTruthy()
  }));

  it('[ Button-Click ] - disabled button "true" ', (() => {
    component.disabledCondition = false;
    fixture.detectChanges()
    expect(buttonHTML.disabled).toBeFalsy()
  }));

});













/*

let de : DebugElement;
de=fixture.debugElement;
const h1 = de.query(By.css('h1));
const button = de.query(By.css('#buttonId));
btn.triggerEventHandler('click',{})

*/
