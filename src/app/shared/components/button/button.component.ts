import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
    @Input() label: string;
    @Output() onClick = new EventEmitter<any>();
    @Input() type:string
    @Input() disabledCondition:boolean
    
    onClickButton() {
        this.onClick.emit();
    }
    
    constructor() { }

    ngOnInit(): void {
      // console.log(this.disabledCondition)
    }

}
