import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveValidationComponent } from '../reactive-validation/reactive-validation.component';

import { InputDatePickerComponent } from './input-date-picker.component';

describe('InputDatePickerComponent', () => {
  let component: InputDatePickerComponent;
  let fixture: ComponentFixture<InputDatePickerComponent>;
  let testForm;
  let htmlCompiled;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ RouterTestingModule, ReactiveFormsModule ],
      declarations: [ InputDatePickerComponent,ReactiveValidationComponent ]
    })
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputDatePickerComponent);
    component = fixture.debugElement.componentInstance;
    htmlCompiled = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  describe('Date input field',()=>{
    let dateControl;
    let dateInput;

    beforeAll(()=> {
      testForm = new FormGroup({
        'date': new FormControl(
          null,
          [ Validators.required ]
      )});
    });

    beforeEach(()=>{
      component.label = 'date';
      component.control = testForm.controls['date'];
      component.defaultDate = '2000-05-05'
      component.type = 'date';
      component.fieldId = 'date';
      dateControl = component.control;
      component.ngOnInit();
      fixture.detectChanges();
      dateInput = htmlCompiled.querySelector('input');
    });
    
    it('[Date-check] - check HTML values',()=> {
      expect(dateInput).toBeTruthy();
      let label = htmlCompiled.querySelector('label').textContent.trim();
      expect(label).toEqual(component.label);
      expect(dateInput.id).toEqual(component.fieldId);
      expect(dateInput.type).toEqual(component.type);
      dateControl.setValue('2000-05-05');
      expect(dateInput.value).toEqual(dateControl.value);
      expect(dateInput.value).toEqual(component.defaultDate)
    });

    it('[Date-check] - invalid blank value',()=>{
      dateControl.setValue('');
      expect(dateControl.errors['required']).toBeTruthy();
      expect(dateControl.invalid).toBeTruthy();
    });

   
    it('[Date-check] - invalid value',()=>{
      component.defaultDate = "12-1212-1" 
      fixture.detectChanges()
      expect(component.defaultDate).not.toEqual(dateInput.value);
      expect(dateInput.value).toEqual('')
    });
    // valid testcase
    it('[Date-check] - valid value',()=>{
      component.defaultDate = "2000-05-05" 
      fixture.detectChanges()
      expect(component.defaultDate).toEqual(dateInput.value);
      expect(dateInput.value).toEqual('2000-05-05')
    });
  });
});
