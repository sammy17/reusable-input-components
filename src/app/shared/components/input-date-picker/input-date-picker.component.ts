import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-input-date-picker',
  templateUrl: './input-date-picker.component.html',
  styleUrls: ['./input-date-picker.component.scss']
})
export class InputDatePickerComponent implements OnInit {
@Input() fieldId:string;
@Input() label:string;
@Input() control:AbstractControl;
@Input() type:'date';
@Input() defaultDate:string
  constructor() { }

  ngOnInit(): void {
    console.log(this.defaultDate)
  }

}
