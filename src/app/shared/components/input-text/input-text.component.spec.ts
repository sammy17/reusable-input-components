import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { InputTextComponent } from './input-text.component';
import { ReactiveValidationComponent } from '../reactive-validation/reactive-validation.component'
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ValidationService } from '../../services/validation.service'

describe('Component: Reactive-Input',()=>{
    let fixture: ComponentFixture<InputTextComponent>;
    let component: InputTextComponent;
    let testForm;
    let htmlCompiled;

    beforeEach(async()=>{
      TestBed.configureTestingModule({
        imports: [ RouterTestingModule, ReactiveFormsModule ],
        declarations: [ InputTextComponent, ReactiveValidationComponent ],
        providers: [ ValidationService ]
      })
    })

    beforeEach(()=>{
      fixture =  TestBed.createComponent(InputTextComponent);
      component = fixture.debugElement.componentInstance;
      htmlCompiled = fixture.debugElement.nativeElement;
    });

    it('should create reactive input',()=> {
      expect(component).toBeTruthy();
    });

    describe('Email input field',()=>{
      let emailControl;
      let emailInput;
      let validationService;
      beforeAll(()=> {
         validationService = fixture.debugElement.injector.get(ValidationService);
        testForm = new FormGroup({
          'userEmail': new FormControl(
            null,
            [ Validators.required, Validators.email, validationService.forbiddenTextPattern ]
        )});
      });

      beforeEach(()=>{
        component.label = 'Email Address';
        component.control = testForm.controls['userEmail'];
        component.type = 'email';
        component.fieldId = 'userEmail';
        emailControl = component.control;
        component.ngOnInit();
        fixture.detectChanges();
        emailInput = htmlCompiled.querySelector('input');

      });

      it('[Email-check] - check HTML values',()=> {
        expect(emailInput).toBeTruthy();
        let label = htmlCompiled.querySelector('label').textContent.trim();
        expect(label).toEqual(component.label);
        expect(emailInput.id).toEqual(component.fieldId);
        expect(emailInput.type).toEqual(component.type);
        emailControl.setValue('testEmail');
        expect(emailInput.value).toEqual(emailControl.value);
      });

      it('[Email-check] - invalid blank value',()=>{
        emailControl.setValue('');
        expect(emailControl.errors['required']).toBeTruthy();
        expect(emailControl.invalid).toBeTruthy();
      });

      it('[Email-check] - invalid script tags',()=>{
        emailControl.setValue('<script>');
        expect(emailControl.errors['forbiddenTextPattern']).toBeTruthy();
        expect(emailControl.invalid).toBeTruthy();
      });

      it('[Email-check] - invalid email',()=>{
        emailControl.setValue('testEmail');
        expect(emailControl.errors['email']).toBeTruthy();
        expect(emailControl.invalid).toBeTruthy();
      });
      
      it('[Email-check] - valid email',()=>{
        emailControl.setValue('test@test.com');

        expect(emailControl.valid).toBeTruthy();
        expect(emailControl.errors).toBeNull();
      });
    });

    describe('Simple input field',()=>{
      let inputControl;
      let inputField;

      beforeAll(()=>{
        let validationService = fixture.debugElement.injector.get(ValidationService);
        testForm = new FormGroup({
          'userName': new FormControl(
            null,
            [ Validators.required, validationService.forbiddenTextPattern ]
        )});
      });

      beforeEach(()=>{
        component.label = 'Username';
        component.control = testForm.controls['userName'];;
        component.type = 'text';
        component.fieldId = 'userName';
        inputControl = component.control;

        component.ngOnInit();
        fixture.detectChanges();
        inputField = htmlCompiled.querySelector('input');
      });

      it('[Simple input-check] - check HTML values',()=> {
        // expect(inputField.style.webkitTextSecurity).toBe("disc")
        expect(inputField).toBeTruthy();
        let label = htmlCompiled.querySelector('label').textContent.trim();
        expect(label).toEqual(component.label);
        expect(inputField.id).toEqual(component.fieldId);
        expect(inputField.type).toEqual(component.type);
        inputControl.setValue('testUser');
        expect(inputField.value).toEqual(inputControl.value);
      });

      it('[Simple input-check] - invalid blank value',()=>{
        inputControl.setValue('');
        expect(inputControl.errors['required']).toBeTruthy();
        expect(inputControl.invalid).toBeTruthy();
      });

      it('[Simple input-check] - invalid script tags',()=>{
        inputControl.setValue('<script>');
        expect(inputControl.errors['forbiddenTextPattern']).toBeTruthy();
        expect(inputControl.invalid).toBeTruthy();
      });

      it('[Simple input-check] - valid value',()=>{
        inputControl.setValue('testUser');
        // expect(inputControl.errors).toBeNull();
      });

    });
    
});



