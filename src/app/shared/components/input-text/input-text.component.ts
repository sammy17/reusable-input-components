import { Component, OnInit, DoCheck, Input }  from '@angular/core';
import { AbstractControl }                    from '@angular/forms';

@Component({
  selector: 'app-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss']
})

export class InputTextComponent implements OnInit, DoCheck {

  @Input() label: string | null = null;
  @Input() fieldId: string | null = null;
  @Input() type: 'text' | 'email' | 'password' = 'text';

  @Input() control: AbstractControl | null = null;

  validationErrors: object = null;

  ngOnInit() {}

  ngDoCheck() {
    this.validationErrors = this.control.touched && this.control.invalid ? this.control['errors'] : null;
  }
}
