import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { LabelComponent } from './label.component';

describe('LabelComponent', () => {
  let component: LabelComponent;
  let fixture: ComponentFixture<LabelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LabelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not have email text content',()=>{
    let label=fixture.debugElement.query(By.css('#label'))
    fixture.detectChanges()
    expect(label.nativeElement.textContent).toEqual('')
  })

  
  it('should have email text content',()=>{
    component.label = "Email";
    let label=fixture.debugElement.query(By.css('#label'))
    fixture.detectChanges()
    expect(label.nativeElement.textContent).toEqual('Email')
  })
});
