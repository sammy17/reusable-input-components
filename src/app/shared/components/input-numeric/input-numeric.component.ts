import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-input-numeric',
  templateUrl: './input-numeric.component.html',
  styleUrls: ['./input-numeric.component.scss']
})
export class InputNumericComponent implements OnInit {
@Input() label:string;
@Input() fieldId:string;
@Input() control:AbstractControl;
@Input() type: 'number';
validationErrors: object = null;

  constructor() { }

  ngOnInit(): void {
  }
  
  ngDoCheck() {
    this.validationErrors = this.control.touched && this.control.invalid ? this.control['errors'] : null;
  }

}
