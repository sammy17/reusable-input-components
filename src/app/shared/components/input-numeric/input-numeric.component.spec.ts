import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveValidationComponent } from '../reactive-validation/reactive-validation.component';

import { InputNumericComponent } from './input-numeric.component';

describe('InputNumericComponent', () => {
    let component: InputNumericComponent;
    let fixture: ComponentFixture<InputNumericComponent>;
    let htmlCompiled;
    let testForm;

    beforeEach(async () => {
      TestBed.configureTestingModule({
        imports: [ RouterTestingModule, ReactiveFormsModule ],
        declarations: [ InputNumericComponent, ReactiveValidationComponent ]
      })
    });

    beforeEach(() => {
      fixture = TestBed.createComponent(InputNumericComponent);
      component = fixture.debugElement.componentInstance;
      htmlCompiled = fixture.debugElement.nativeElement;
    });

    it('should create reactive number input', () => {
      expect(component).toBeTruthy();
    });
    describe('number input field',()=>{
      let numberControl;
      let numberInput;

      beforeAll(()=> {
      
        testForm = new FormGroup({
          'numeric': new FormControl(
            null,
            [ Validators.required]
        )});
      });

      beforeEach(()=>{
        component.label = 'Number input';
        component.control = testForm.controls['numeric']
        component.type = 'number'
        component.fieldId = 'numeric';
        numberControl = component.control;
        component.ngOnInit();
        fixture.detectChanges();
        numberInput = htmlCompiled.querySelector('input');
      });

      it(('[Number-check] - only number allowed'),()=>{
        numberControl.setValue('text')
        fixture.detectChanges();
        expect(numberInput.value).toBe('')
      })

      it('[Number-check] - invalid blank value',()=>{
        numberControl.setValue('');
        expect(numberControl.errors['required']).toBeTruthy();
        expect(numberControl.invalid).toBeTruthy();
      });

      it('[Number-check] - label check',()=>{
        expect(htmlCompiled.querySelector('label').textContent).toEqual(component.label);
      });

      it('[Number-check] - valid value',()=>{
        numberControl.setValue('12')
        fixture.detectChanges()
        expect(numberInput.value).toEqual('12')
      });

    })
});
