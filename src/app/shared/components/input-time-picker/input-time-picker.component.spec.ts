import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveValidationComponent } from '../reactive-validation/reactive-validation.component';

import { InputTimePickerComponent } from './input-time-picker.component';


describe('InputTimePickerComponent', () => {
  let component: InputTimePickerComponent;
  let fixture: ComponentFixture<InputTimePickerComponent>;
  let htmlCompiled;
  let testForm;
  
  beforeEach(async () => {
      TestBed.configureTestingModule({
        imports:[ReactiveFormsModule,RouterTestingModule],
      declarations: [ InputTimePickerComponent, ReactiveValidationComponent ]
    })
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputTimePickerComponent);
    component = fixture.debugElement.componentInstance;
    htmlCompiled = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  describe('Time input field',()=>{
    let timeControl;
    let timeInput;

    beforeAll(()=> {
      testForm = new FormGroup({
        'time': new FormControl(
          null,
          [ Validators.required ]
      )});
    });

    beforeEach(()=>{
      component.label = 'time';
      component.control = testForm.controls['time'];
      component.defaultTime = '19:30'
      component.type = 'time';
      component.fieldId = 'Time';
      timeControl = component.control;
      component.ngOnInit();
      fixture.detectChanges();
      timeInput = htmlCompiled.querySelector('input');
    });
    
    it('[Time-check] - check HTML values',()=> {
      expect(timeInput).toBeTruthy();
      let label = htmlCompiled.querySelector('label').textContent.trim();
      expect(label).toEqual(component.label);
      expect(timeInput.id).toEqual(component.fieldId);
      expect(timeInput.type).toEqual(component.type);
      timeControl.setValue('19:30');
      expect(timeInput.value).toEqual(timeControl.value);
      expect(timeInput.value).toEqual(component.defaultTime)
    });

    it('[Time-check] - invalid blank value',()=>{
      timeControl.setValue('');
      expect(timeControl.errors['required']).toBeTruthy();
      expect(timeControl.invalid).toBeTruthy();
    });
 
    it('[Time-check] - invalid value',()=>{
      component.defaultTime = "123:121"
      fixture.detectChanges()
      expect(component.defaultTime).not.toEqual(timeInput.value);
      expect(timeInput.value).toEqual('')
    });

    //valid testcase

    it('[Date-check] - valid value',()=>{
      component.defaultTime = "19:30"
      fixture.detectChanges()
      expect(component.defaultTime).toEqual(timeInput.value);
      expect(timeInput.value).toEqual('19:30')
    });
  });
});

