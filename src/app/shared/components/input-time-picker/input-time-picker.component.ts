import { Time } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-input-time-picker',
  templateUrl: './input-time-picker.component.html',
  styleUrls: ['./input-time-picker.component.scss']
})
export class InputTimePickerComponent implements OnInit {
  @Input() fieldId:string;
  @Input() label:string;
  @Input() control:AbstractControl;
  @Input() type:'time'
  @Input() defaultTime:string
  constructor() { }

  ngOnInit(): void {
  }

}
