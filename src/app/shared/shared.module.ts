import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { ReactiveValidationComponent } from './components/reactive-validation/reactive-validation.component';
// import { InputTextComponent } from './components/input-text/input-text.component';

import {
  // InputTextComponent,
  InputNumericComponent,
  InputAlphaNumericComponent,
  InputDatePickerComponent,
  InputTimePickerComponent,
  InputDateTimePickerComponent,
  ButtonComponent,
  LabelComponent,
  InputDropdownComponent,
  InputAutoCompleteComponent,
  TableSimpleComponent
} from './components';



@NgModule({
  declarations: [
    // InputTextComponent,
    // InputNumericComponent,
    // LabelComponent,
    // InputDropdownComponent,
    // ReactiveValidationComponent
    // InputAlphaNumericComponent,
    // InputDatePickerComponent,
    // InputTimePickerComponent,
    // InputDateTimePickerComponent,
    // ButtonComponent,
    InputAutoCompleteComponent,
    TableSimpleComponent,
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
