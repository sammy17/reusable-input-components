import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'rate-calculator',
    loadChildren: () => import('./modules/rate-calculator/rate-calculator.module').then(module => module.RateCalculatorModule)
  },
  {
    path: 'orders',
    loadChildren: () => import('./modules/orders/orders.module').then(module => module.OrdersModule)
  },
  {
    path: 'trips',
    loadChildren: () => import('./modules/trips/trips.module').then(module => module.TripsModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./modules/dashboard/dashboard.module').then(module => module.DashboardModule)
  },
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
