import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TripsRoutingModule } from './trips-routing.module';
import { TripsContainerComponent } from './trips-container/trips-container.component';


@NgModule({
  declarations: [TripsContainerComponent],
  imports: [
    CommonModule,
    TripsRoutingModule
  ]
})
export class TripsModule { }
