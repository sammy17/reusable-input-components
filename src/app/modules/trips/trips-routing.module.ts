import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TripsContainerComponent } from './trips-container/trips-container.component';

const routes: Routes = [
  {
    path: '',
    component: TripsContainerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TripsRoutingModule { }
