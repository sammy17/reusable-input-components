import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdersContainerComponent } from './orders-container/orders-container.component';

const routes: Routes = [
  {
    path: '',
    component: OrdersContainerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
