import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RateCalculatorRoutingModule } from './rate-calculator-routing.module';
import { RateCalculatorContainerComponent } from './rate-calculator-container/rate-calculator-container.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputTextComponent } from '../../shared/components/input-text/input-text.component';
import { ReactiveValidationComponent } from '../../shared/components/reactive-validation/reactive-validation.component';
import { LabelComponent } from 'src/app/shared/components';
import { InputDropdownComponent }  from 'src/app/shared/components/input-dropdown/input-dropdown.component'
import { InputNumericComponent } from 'src/app/shared/components/input-numeric/input-numeric.component'
import { ButtonComponent } from 'src/app/shared/components/button/button.component'
import { InputAlphaNumericComponent } from 'src/app/shared/components/input-alpha-numeric/input-alpha-numeric.component'
import { InputDatePickerComponent } from 'src/app/shared/components/input-date-picker/input-date-picker.component'
import { InputDateTimePickerComponent } from 'src/app/shared/components/input-date-time-picker/input-date-time-picker.component'
import { InputTimePickerComponent } from 'src/app/shared/components/input-time-picker/input-time-picker.component'
@NgModule({
  declarations: [
  	RateCalculatorContainerComponent,
  	InputTextComponent,
    ReactiveValidationComponent,
    LabelComponent,
    InputDropdownComponent,
    InputNumericComponent,
    ButtonComponent,
    InputAlphaNumericComponent,
    InputDatePickerComponent,
    InputDateTimePickerComponent,
    InputTimePickerComponent,
  ],
  imports: [
    CommonModule,
    RateCalculatorRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule
  ]
})
export class RateCalculatorModule { }
