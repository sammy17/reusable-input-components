import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RateCalculatorContainerComponent } from './rate-calculator-container/rate-calculator-container.component';

const routes: Routes = [
  {
    path: '',
    component: RateCalculatorContainerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RateCalculatorRoutingModule { }
