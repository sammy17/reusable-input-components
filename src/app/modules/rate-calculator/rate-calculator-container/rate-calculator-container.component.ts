import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'; 
import { ALPHA_NUMERIC_PATTERN } from 'src/app/shared/constants/validation-patterns-list';
import { InputTextComponent } from '../../../shared/components/input-text/input-text.component';
import { ValidationService } from '../../../shared/services/validation.service'

@Component({
  selector: 'app-rate-calculator-container',
  templateUrl: './rate-calculator-container.component.html',
  styles: [
  ]
})
export class RateCalculatorContainerComponent implements OnInit, OnChanges {
values:string[]=["Save Data","Fetch Data","Delete Data"]
defaultDate = "2000-05-05"
defaultDateTime = "2018-06-12T19:30"
defaultTime = "10:10"
  constructor(private formBuilder: FormBuilder,private validationService:ValidationService) {}

  title:string = 'Welcome to my awesome reactive forms';

  form: FormGroup = new FormGroup({
    'miles': new FormControl(
      '6700',
      [Validators.required]),
    'deadhead': new FormControl(
      '7000',
      [Validators.required]),
    'email': new FormControl(
      '',
      [Validators.required,Validators.email,this.validationService.forbiddenTextPattern]) ,
    'password': new FormControl(
      '',
      [Validators.required,this.validationService.forbiddenTextPattern]) ,
    'dropdown':new FormControl(
      this.values,
      Validators.required),
    'numeric':new FormControl(
      null,
      Validators.required),
    'alphaNumeric':new FormControl(
      null,
      [Validators.required,Validators.pattern(ALPHA_NUMERIC_PATTERN)]),
    'date':new FormControl('',
      []),
    'dateAndTime': new FormControl('',
      []),
    'time': new FormControl('',
      [])
  });

  ngOnInit(): void {
    console.log(this.form)
  }

  ngOnChanges():void{
    console.log(this.form)
  }
  get miles() {
    return this.form.get('miles');
  }

  get deadhead() {
    return this.form.get('deadhead');
  }

  onSubmit() {
    if (this.form.valid) {
      // Working on your validated form data
    } else {
      this.validationService.markAllFormFieldsAsTouched(this.form);
    }
  }
}
