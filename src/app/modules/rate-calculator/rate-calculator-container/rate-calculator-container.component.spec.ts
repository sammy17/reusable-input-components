import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { RateCalculatorContainerComponent } from './rate-calculator-container.component';

import { By } from '@angular/platform-browser';

describe('RateCalculatorContainerComponent', () => {
  let component: RateCalculatorContainerComponent;
  let fixture: ComponentFixture<RateCalculatorContainerComponent>;
  let app;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RateCalculatorContainerComponent ],
      imports: [ReactiveFormsModule],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RateCalculatorContainerComponent);
    component = fixture.componentInstance;
    app = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    fixture = TestBed.createComponent(RateCalculatorContainerComponent);
    component = fixture.componentInstance;
    app = fixture.debugElement.componentInstance;


    expect(component).toBeTruthy();
  });

  it(`should have as title 'my-awesome-reactive-forms'`, () => {
    fixture = TestBed.createComponent(RateCalculatorContainerComponent);
    component = fixture.componentInstance;
    app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Welcome to my awesome reactive forms');
  });

  it('[Email-check] - should check users correct email is invalid', () => {

    fixture = TestBed.createComponent(RateCalculatorContainerComponent);
    component = fixture.componentInstance;
    app = fixture.debugElement.componentInstance;
    let email = app.form.controls['miles'];
    expect(email.invalid).toBeTruthy();
    expect(email.pristine).toBeTruthy();
    expect(email.errors['required']).toBeTruthy();
    email.setValue('abc');
    expect(email.valid).toBeTruthy();       
  });

  it('[Email-check] - should check users correct email is entered', () => {
    fixture = TestBed.createComponent(RateCalculatorContainerComponent);
    component = fixture.componentInstance;
    app = fixture.debugElement.componentInstance;


    let email = app.form.controls['miles'];
    email.setValue('6700');
    expect(email.errors).toBeNull();
  });

});

